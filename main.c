#include <stdlib.h>
#include <string.h>

#include "xmc4800.h"
#include "xmc4800_relax_ecat_kit.h"

#include "debug.h"

#include "mcu_setup.h"

#include "network.h"
#include "mqtt.h"
#include "dmx_application.h"
#include "dali_application.h"

uint8_t res[1];

static void dmx_process(char*, char*, char* data);
static void dali_process(char* daliloop, char* data, char* action);

OsTask* task_mqttReceive;
OsTask* task_mqttSend;

osMailQDef 		(mqttIn_DMX, 255, mqttIn_DMX_Data);
osMailQId			mqttIn_DMX_id;
osMailQDef 		(mqttIn_DALI, 5, mqttIn_DALI_Data);
osMailQId			mqttIn_DALI_id;
osMailQDef		(mqttOut_DALI, 5, mqttOut_DALI_Data);
osMailQId			mqttOut_DALI_id;

extern OsTask* dali_initAddressingProcess_task;

void mqtt_message_arrived(MqttClientContext* mqtt_ctx, const char* topic, const uint8_t* message, size_t length)
{
	TRACE_INFO("\n\rmqtt message (%s [%i]) arrived for topic: %s\n\r", message, length, topic);
		
	char* t_ctx;
	char t[strlen(topic)];
	int i=0;
	while (*topic != '\0') t[i++] = *topic++;
	
	// dispatch
	// supported topics: 
	//  dmx/<universe>/fixture1/ - data: byte(ch1)-byte(ch2)-byte(ch3)-...
	//  dali/addressing/  			 - data: *ignored*
	//  dali/command/						 - byte(address)-byte(command)
	//  dali/directpower				 - byte(address)-byte(data)
	char* protocol = strtok_r(t, "/", &t_ctx);
	
	char* data = malloc(length+1);
		
	strncpy(data, (const char*)message, length);
	data[length] = '\0';
	
	if (!strcmp(protocol, "dmx"))
	{
		char* universe = strtok_r(NULL, "/", &t_ctx);
		char* fixture = strtok_r(NULL, "/", &t_ctx);		
		dmx_process(universe, fixture, data);
	}
	else if (!strcmp(protocol, "dali"))
	{
		//char* daliloop = strtok_r(NULL, "/", &t_ctx);
		char* action = strtok_r(NULL, "/", &t_ctx);
		dali_process("", data, action);
	}
}

void dmx_process(char* universe, char* fixture_name, char* data)
{	
	mqttIn_DMX_Data* in = (mqttIn_DMX_Data*)osMailCAlloc(mqttIn_DMX_id, osWaitForever);	
	in->fixture_name = fixture_name;	
	in->channels = data;
	osMailPut(mqttIn_DMX_id, in);
}

void dali_process(char* daliloop, char mqttdata[], char* action)
{
	if (!strcmp(action, "addressing"))
	{
		osSignalSet(dali_initAddressingProcess_task,	DALI_ADDRESSING_EVENT); 
	} 
	else
	{
		char* mqttdata_ctx;	
		uint8_t address;
		char* address_ctx;
		uint8_t data;
		char* data_ctx;

		address = strtoul(strtok_r(mqttdata, "-", &mqttdata_ctx), &address_ctx, 10);
		data = strtoul(strtok_r(NULL, "-", &mqttdata_ctx), &data_ctx, 10);

		mqttIn_DALI_Data* in = (mqttIn_DALI_Data*)osMailCAlloc(mqttIn_DALI_id, osWaitForever);
		in->address = address;
		in->data = data;
		
		if (!strcmp(action, "command"))
		{			
			in->action = ACTION_COMMAND;
		}
		else if (!strcmp(action, "directpower"))
		{
			in->action = ACTION_DIRECTPWR;
		}
				
		osMailPut(mqttIn_DALI_id, in);
	}
}

void mqttReceiveTask(void *mqtt_ctx)
{
	while (1)
	{
		mqttClientProcessEvents((MqttClientContext*)mqtt_ctx, osWaitForever);		
	}
}

void mqttSendTask(void *mqtt_ctx)
{
	while (1)
	{
		osEvent mqttOut_mail = osMailGet(mqttOut_DALI_id, osWaitForever);
		if (mqttOut_mail.status == osEventMail)
		{
			mqttOut_DALI_Data* out = (mqttOut_DALI_Data*)mqttOut_mail.value.p;
			char str[4];
			sprintf(str, "0x%x", out->result);

			osMailFree(mqttOut_DALI_id, out);
			
			mqttClientPublish(mqtt_ctx, "dali/result", str, 4, MQTT_QOS_LEVEL_2, 0);
		 		
			
			TRACE_INFO("mqtt message sent to dali/result: %s\r\n", str);
		}	
	}
}

int main(void) 
{	
	osInitKernel();
	//Configure debug UART
	debugInit(115200);

	TRACE_INFO("\r\n");
	TRACE_INFO("*********************\r\n");
	TRACE_INFO("**** INFO WINDOW ****\r\n");
	TRACE_INFO("*********************\r\n");	
	TRACE_INFO("\r\n");

	error_t error = NO_ERROR;
	MqttClientContext mqtt_ctx;

	error = network_initialize();
	if (error) 
		TRACE_INFO("Failed to initialize TCP/IP stack: %i\r\n", error);
	else
		TRACE_INFO("TCP/IP stack initialized\r\n");

	error = mqtt_initialize(&mqtt_ctx);
	if (error) 
		TRACE_INFO("Failed to initialize MQTT: %i\r\n", error);
	else
		TRACE_INFO("MQTT initialized\r\n");
	
	mqtt_registerPublishCallback(&mqtt_ctx, mqtt_message_arrived);	
	
	TRACE_INFO("Connecting to MQTT broker...");
	error = mqtt_connect(&mqtt_ctx);
	if (error)
		TRACE_INFO("failed: %i\r\n", error);
	else
		TRACE_INFO("connected\r\n");		
		
	mqttIn_DMX_id = osMailCreate(osMailQ(mqttIn_DMX), NULL);
	mqttIn_DALI_id = osMailCreate(osMailQ(mqttIn_DALI), NULL);
	mqttOut_DALI_id = osMailCreate(osMailQ(mqttOut_DALI), NULL);
	task_mqttReceive = osCreateTask("MQTT_RECEIVE", mqttReceiveTask, &mqtt_ctx, 50, OS_TASK_PRIORITY_NORMAL);
	task_mqttSend = osCreateTask("MQTT_SEND", mqttSendTask, &mqtt_ctx, 50, OS_TASK_PRIORITY_NORMAL);	
	
	/////////////////////////////////
	/// INITIALIZE MCU PERIPHERALS
	/////////////////////////////////
	TRACE_INFO("Initialising peripherals...");
	enum STATUS status = mcu_initialize();
	if (status == SUCCESS)
		TRACE_INFO("success\r\n");
	else
		TRACE_INFO("failed\r\n");
	
	////////////////////////////////
	// DMX initialisation
	////////////////////////////////
	char_t* topic_dmxFixture1 = "dmx/A/fixture1/";
	char_t* topic_dmxFixture2 = "dmx/A/fixture2/";
	TRACE_INFO("Subscribing to topic %s...", topic_dmxFixture1);
	error = mqtt_subscribe(&mqtt_ctx, topic_dmxFixture1);
	if (error) 
		TRACE_INFO("failed: %i\r\n", error);
	else
		TRACE_INFO("subscribed\r\n");
	TRACE_INFO("Subscribing to topic %s...", topic_dmxFixture2);
	error = mqtt_subscribe(&mqtt_ctx, topic_dmxFixture2);
	if (error) 
		TRACE_INFO("failed: %i\r\n", error);
	else
		TRACE_INFO("subscribed\r\n");
	
	dmx_initialize_application();
			
	////////////////////////////////
	// DALI initialisation
	////////////////////////////////
	
	char_t* topic_daliAddressing = "dali/addressing/";
	char_t* topic_daliCommand = "dali/command/";
	char_t* topic_daliDirectPower = "dali/directpower/";
	TRACE_INFO("Subscribing to topic %s...", topic_daliAddressing);
	error = mqtt_subscribe(&mqtt_ctx, topic_daliAddressing);
	if (error) 
		TRACE_INFO("failed: %i\r\n", error);
	else
		TRACE_INFO("subscribed\r\n");
	TRACE_INFO("Subscribing to topic %s...", topic_daliCommand);
	error = mqtt_subscribe(&mqtt_ctx, topic_daliCommand);
	if (error) 
		TRACE_INFO("failed: %i\r\n", error);
	else
		TRACE_INFO("subscribed\r\n");
	TRACE_INFO("Subscribing to topic %s...", topic_daliDirectPower);
	error = mqtt_subscribe(&mqtt_ctx, topic_daliDirectPower);
	if (error) 
		TRACE_INFO("failed: %i\r\n", error);
	else
		TRACE_INFO("subscribed\r\n");
	
	dali_initialize_application(task_mqttSend);
				
	osStartKernel();
	
	osThreadTerminate(osThreadGetId());
}
