#include <stdint.h>

typedef void (* volatile callback_void)();
typedef void (* volatile callback_uint8)(uint8_t);



enum MANCHESTER_RX_PULSETYPE
{
	SHORT_PULSE, LONG_PULSE, OTHER_PULSE
};

void manchester_initialize(void);

void initialize_tx(uint16_t, callback_void, callback_void);
void start_tx(void);
void stop_tx(void);

void initialize_rx(callback_void f, callback_uint8 f2, callback_void f3);
void start_rx(void);
void stop_rx(void);
