typedef struct 
{
	char* fixture_name;
	char* channels;
} mqttIn_DMX_Data;

struct dmx_fixture
{
	char* name;
	int first_channel;
	int number_of_channels;
};

extern struct dmx_fixture fixtures[];

struct dmx_fixture* retrieve_fixture(char* fixture_name);
void dmx_initialize_application(void);
void dmx_register_fixture(char* name, int first_channel, int number_channels);
