#include <stdint.h>
#include <stdio.h>

typedef struct
{
	char*	description;
	uint8_t expectAnswer : 1;
	uint8_t sendTwice    : 1;
} commandinfo_t;

/*
typedef union 
{
	struct
	{
		uint8_t address_type : 1;
		uint8_t address		 : 6;
		uint8_t selector	 : 1;
	};
	struct
	{
		uint8_t one		 : 1;		
		uint8_t specialcmd : 4;
		uint8_t special	 : 3;		
	};
} address_t;
*/
typedef struct
{	
	uint8_t	datapart;
	uint8_t addresspart;
	//address_t 	  addresspart;
} forwardframe_t;

typedef struct
{
	commandinfo_t info;
	union
	{
		forwardframe_t framedata;
		uint16_t 	raw;
	};
} dalicommand_t;

/*
dalicommand_t dali_commands[256] =
{
	{.info = {"off", 0, 0}, .framedata = {.datapart = 0x00}},
	{.info = {"up", 0, 0}, .framedata = {.datapart = 0x01}}, {.info = {"down", 0, 0}, .framedata = {.datapart = 0x02}}, 
	{.info = {"step up", 0, 0}, .framedata = {.datapart = 0x03}}, {.info = {"step down", 0, 0}, .framedata = {.datapart = 0x04}}, 
	{.info = {"recall max lvl", 0, 0}, .framedata = {.datapart = 0x05}}, {.info = {"recall min lvl", 0, 0}, .framedata = {.datapart = 0x06}}, 
	{.info = {"step down off", 0, 0}, .framedata = {.datapart = 0x07}}, {.info = {"on step up", 0, 0}, .framedata = {.datapart = 0x08}}, 		
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{.info = {"goto scene 1", 0, 0}, .framedata = {.datapart = 0x10}}, {.info = {"goto scene 2", 0, 0}, .framedata = {.datapart = 0x11}},
	{.info = {"goto scene 3", 0, 0}, .framedata = {.datapart = 0x12}}, {.info = {"goto scene 4", 0, 0}, .framedata = {.datapart = 0x13}},
	{.info = {"goto scene 5", 0, 0}, .framedata = {.datapart = 0x14}}, {.info = {"goto scene 6", 0, 0}, .framedata = {.datapart = 0x15}},
	{.info = {"goto scene 7", 0, 0}, .framedata = {.datapart = 0x16}}, {.info = {"goto scene 8", 0, 0}, .framedata = {.datapart = 0x17}},
	{.info = {"goto scene 9", 0, 0}, .framedata = {.datapart = 0x18}}, {.info = {"goto scene 10", 0, 0}, .framedata = {.datapart = 0x19}},
	{.info = {"goto scene 11", 0, 0}, .framedata = {.datapart = 0x1A}}, {.info = {"goto scene 12", 0, 0}, .framedata = {.datapart = 0x1B}},
	{.info = {"goto scene 13", 0, 0}, .framedata = {.datapart = 0x1C}}, {.info = {"goto scene 14", 0, 0}, .framedata = {.datapart = 0x1D}},
	{.info = {"goto scene 15", 0, 0}, .framedata = {.datapart = 0x1E}}, {.info = {"goto scene 16", 0, 0}, .framedata = {.datapart = 0x1F}},
	{.info = {"reset", 0, 1}, .framedata = {.datapart = 0x20}},
	{.info = {"store actual lvl in dtr", 0, 1}, .framedata = {.datapart = 0x21}},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{.info = {"store DTR as max lvl", 0, 1}, .framedata = {.datapart = 0x2A}},
	{.info = {"store DTR as min lvl", 0, 1}, .framedata = {.datapart = 0x2B}},
	{.info = {"store DTR as sys fail lvl", 0, 1}, .framedata = {.datapart = 0x2C}},
	{.info = {"store DTR as power on lvl", 0, 1}, .framedata = {.datapart = 0x2D}},
	{.info = {"store DTR as fade time", 0, 1}, .framedata = {.datapart = 0x2E}},
	{.info = {"store DTR as fade rate", 0, 1}, .framedata = {.datapart = 0x2F}},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{.info = {"store DTR as scene 1", 0, 1}, .framedata = {.datapart = 0x40}}, {.info = {"store DTR as scene 2", 0, 1}, .framedata = {.datapart = 0x41}},
	{.info = {"store DTR as scene 3", 0, 1}, .framedata = {.datapart = 0x42}}, {.info = {"store DTR as scene 4", 0, 1}, .framedata = {.datapart = 0x43}},
	{.info = {"store DTR as scene 5", 0, 1}, .framedata = {.datapart = 0x44}}, {.info = {"store DTR as scene 6", 0, 1}, .framedata = {.datapart = 0x45}},
	{.info = {"store DTR as scene 7", 0, 1}, .framedata = {.datapart = 0x46}}, {.info = {"store DTR as scene 8", 0, 1}, .framedata = {.datapart = 0x47}},
	{.info = {"store DTR as scene 9", 0, 1}, .framedata = {.datapart = 0x48}}, {.info = {"store DTR as scene 10", 0, 1}, .framedata = {.datapart = 0x49}},
	{.info = {"store DTR as scene 11", 0, 1}, .framedata = {.datapart = 0x4A}}, {.info = {"store DTR as scene 12", 0, 1}, .framedata = {.datapart = 0x4B}},
	{.info = {"store DTR as scene 13", 0, 1}, .framedata = {.datapart = 0x4C}}, {.info = {"store DTR as scene 14", 0, 1}, .framedata = {.datapart = 0x4D}},
	{.info = {"store DTR as scene 15", 0, 1}, .framedata = {.datapart = 0x4E}}, {.info = {"store DTR as scene 16", 0, 1}, .framedata = {.datapart = 0x4F}},
	{.info = {"remove from scene 1", 0, 1}, .framedata = {.datapart = 0x50}}, {.info = {"remove from scene 2", 0, 1}, .framedata = {.datapart = 0x51}},
	{.info = {"remove from scene 3", 0, 1}, .framedata = {.datapart = 0x52}}, {.info = {"remove from scene 4", 0, 1}, .framedata = {.datapart = 0x53}},
	{.info = {"remove from scene 5", 0, 1}, .framedata = {.datapart = 0x54}}, {.info = {"remove from scene 6", 0, 1}, .framedata = {.datapart = 0x55}},
	{.info = {"remove from scene 7", 0, 1}, .framedata = {.datapart = 0x56}}, {.info = {"remove from scene 8", 0, 1}, .framedata = {.datapart = 0x57}},
	{.info = {"remove from scene 9", 0, 1}, .framedata = {.datapart = 0x58}}, {.info = {"remove from scene 10", 0, 1}, .framedata = {.datapart = 0x59}},
	{.info = {"remove from scene 11", 0, 1}, .framedata = {.datapart = 0x5A}}, {.info = {"remove from scene 12", 0, 1}, .framedata = {.datapart = 0x5B}},
	{.info = {"remove from scene 13", 0, 1}, .framedata = {.datapart = 0x4C}}, {.info = {"remove from scene 14", 0, 1}, .framedata = {.datapart = 0x4D}},
	{.info = {"remove from scene 15", 0, 1}, .framedata = {.datapart = 0x4E}}, {.info = {"remove from scene 16", 0, 1}, .framedata = {.datapart = 0x4F}},
	{.info = {"add to group 1", 0, 1}, .framedata = {.datapart = 0x60}}, {.info = {"add to group 2", 0, 1}, .framedata = {.datapart = 0x61}},
	{.info = {"add to group 3", 0, 1}, .framedata = {.datapart = 0x62}}, {.info = {"add to group 4", 0, 1}, .framedata = {.datapart = 0x63}},
	{.info = {"add to group 5", 0, 1}, .framedata = {.datapart = 0x64}}, {.info = {"add to group 6", 0, 1}, .framedata = {.datapart = 0x65}},
	{.info = {"add to group 7", 0, 1}, .framedata = {.datapart = 0x66}}, {.info = {"add to group 8", 0, 1}, .framedata = {.datapart = 0x67}},
	{.info = {"add to group 9", 0, 1}, .framedata = {.datapart = 0x68}}, {.info = {"add to group 10", 0, 1}, .framedata = {.datapart = 0x69}},
	{.info = {"add to group 11", 0, 1}, .framedata = {.datapart = 0x6A}}, {.info = {"add to group 12", 0, 1}, .framedata = {.datapart = 0x6B}},
	{.info = {"add to group 13", 0, 1}, .framedata = {.datapart = 0x6C}}, {.info = {"add to group 14", 0, 1}, .framedata = {.datapart = 0x6D}},
	{.info = {"add to group 15", 0, 1}, .framedata = {.datapart = 0x6E}}, {.info = {"add to group 16", 0, 1}, .framedata = {.datapart = 0x6F}},
	{.info = {"remove from group 1", 0, 1}, .framedata = {.datapart = 0x70}}, {.info = {"remove from group 2", 0, 1}, .framedata = {.datapart = 0x71}},
	{.info = {"remove from group 3", 0, 1}, .framedata = {.datapart = 0x72}}, {.info = {"remove from group 4", 0, 1}, .framedata = {.datapart = 0x73}},
	{.info = {"remove from group 5", 0, 1}, .framedata = {.datapart = 0x74}}, {.info = {"remove from group 6", 0, 1}, .framedata = {.datapart = 0x75}},
	{.info = {"remove from group 7", 0, 1}, .framedata = {.datapart = 0x76}}, {.info = {"remove from group 8", 0, 1}, .framedata = {.datapart = 0x77}},
	{.info = {"remove from group 9", 0, 1}, .framedata = {.datapart = 0x78}}, {.info = {"remove from group 10", 0, 1}, .framedata = {.datapart = 0x79}},
	{.info = {"remove from group 11", 0, 1}, .framedata = {.datapart = 0x7A}}, {.info = {"remove from group 12", 0, 1}, .framedata = {.datapart = 0x7B}},
	{.info = {"remove from group 13", 0, 1}, .framedata = {.datapart = 0x7C}}, {.info = {"remove from group 14", 0, 1}, .framedata = {.datapart = 0x7D}},
	{.info = {"remove from group 15", 0, 1}, .framedata = {.datapart = 0x7E}}, {.info = {"remove from group 16", 0, 1}, .framedata = {.datapart = 0x7F}},
	{.info = {"store DTR as short address", 0, 1}, .framedata = {.datapart = 0x80}},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, 
	{.info = {"query status", 1, 0}, .framedata = {.datapart = 0x90}},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},
	{NULL}, {NULL}, {NULL}, {NULL}, {NULL}, {NULL},  
	{.info = {"direct power control", 0, 0}}
};
*/
/*
dalicommand_t dali_specialcommands[17] =
{
	{
		.info = {"terminate", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x00, .one = 0x01}
		}		
	},
	{
		.info = {"DTR", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x01, .one = 0x01}
		}
	},
	{
		.info = {"INITIALISE", 0, 1},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x02, .one = 0x01}
		}
	},
	{
		.info = {"RANDOMIZE", 0, 1},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x03, .one = 0x01}
		}
	},
	{
		.info = {"COMPARE", 1, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x04, .one = 0x01}
		}
	},
	{
		.info = {"WITHDRAW", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x05, .one = 0x01}
		}
	},
	{NULL},
	{NULL},
	{
		.info = {"SEARCHADDRH", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x08, .one = 0x01}
		}
	},
	{
		.info = {"SEARCHADDRM", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x09, .one = 0x01}
		}
	},
	{
		.info = {"SEARCHADDRL", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x0A, .one = 0x01}
		}
	},
	{
		.info = {"PROG SHORT ADDR", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x0B, .one = 0x01}
		}
	},
	{
		.info = {"VERIFY SHORT ADDR", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x0C, .one = 0x01}
		}
	},
	{
		.info = {"QUERY SHORT ADDR", 0, 0},
		.framedata = {
			.addresspart = {.special = 0x05, .specialcmd = 0x0D, .one = 0x01}
		}
	},
	NULL,
	NULL,
	NULL
};
*/
