#include "manchester.h"

#include "debug.h"
#include "my_debug.h"

#include "os_port.h"
#include "dave.h"

// in clockticks
#define MIN_TE (845) // 2.250Mhz -> 0.444�s / tick -> 375(416.67-10%) = 845 ticks
#define MAX_TE (1033)
#define MIN_2TE (1690)
#define MAX_2TE (2066)

#define TE_to_us(te) (te*MAX_TE*0.444)

enum MANCHESTER_TX_STATE 
{
	MANCHESTER_TX_IDLE,
	MANCHESTER_TX_START1, MANCHESTER_TX_START2,
	MANCHESTER_TX_BIT1, MANCHESTER_TX_BIT2,
	MANCHESTER_TX_STOP1, MANCHESTER_TX_STOP2, MANCHESTER_TX_STOP3,
	MANCHESTER_TX_END,
	MANCHESTER_TX_ERROR
};

enum MANCHESTER_RX_STATE
{
	MANCHESTER_RX_IDLE,
	MANCHESTER_RX_START_SYNC, MANCHESTER_RX_MIDDLE_SYNC,
	MANCHESTER_RX_START_ONE, MANCHESTER_RX_MIDDLE_ONE,
	MANCHESTER_RX_START_ZERO, MANCHESTER_RX_MIDDLE_ZERO,
	MANCHESTER_RX_STOP,
	MANCHESTER_RX_ERROR
};

volatile enum MANCHESTER_TX_STATE tx_state;
volatile enum MANCHESTER_RX_STATE rx_state;

static enum MANCHESTER_RX_PULSETYPE pulsetype(uint32_t pulsewidth)
{
	if (pulsewidth >= MIN_TE && pulsewidth <= MAX_TE)
	{
		return SHORT_PULSE;
	}
	if (pulsewidth >= MIN_2TE && pulsewidth <= MAX_2TE)
	{
		return LONG_PULSE;
	}
	return OTHER_PULSE;
}

callback_void 	tx_finished_cb;
callback_void		tx_error_cb;
callback_void 	rx_start_cb;
callback_uint8 	rx_finished_cb;
callback_void 	rx_error_cb;

volatile uint16_t 	tx_data;
volatile uint16_t 	tx_data_mask;
volatile uint8_t		rx_data;
volatile uint8_t 		rx_bit_position;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// INTERRUPT HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
volatile static enum MANCHESTER_TX_STATE tx_transitions[10][2] =
{					/*tx_data_mask==0					tx_data_mask != 0			*/
/*IDLE*/	{	MANCHESTER_TX_ERROR,		MANCHESTER_TX_START1	},
/*START1*/{	MANCHESTER_TX_ERROR,		MANCHESTER_TX_START2	},
/*START2*/{	MANCHESTER_TX_ERROR,		MANCHESTER_TX_BIT1		},
/*BIT1*/	{	MANCHESTER_TX_ERROR,		MANCHESTER_TX_BIT2		},
/*BIT2*/	{	MANCHESTER_TX_STOP1,		MANCHESTER_TX_BIT1		},
/*STOP1*/	{	MANCHESTER_TX_STOP2,		MANCHESTER_TX_ERROR		},
/*STOP2*/	{	MANCHESTER_TX_STOP3,		MANCHESTER_TX_ERROR		},
/*STOP3*/	{	MANCHESTER_TX_END,			MANCHESTER_TX_ERROR		},
/*END*/		{	MANCHESTER_TX_IDLE,			MANCHESTER_TX_ERROR		},
/*ERROR*/	{	MANCHESTER_TX_IDLE,			MANCHESTER_TX_IDLE		},
};

void manchester_tx_interruptHandler(void)
{		
	TIMER_ClearEvent(&MANCHESTER_TX_TIMER);

#if ENABLE_DEBUG
	putDebugUInt32("TX:%i->", tx_state);
#endif
	
	tx_state = tx_transitions[tx_state][tx_data_mask == 0 ? 0 : 1];
	
#if ENABLE_DEBUG
	putDebugUInt32("%i, ", tx_state);
#endif
	
	switch (tx_state)
	{				
		case MANCHESTER_TX_START1:				
			break;
		case MANCHESTER_TX_START2:		
		{			
			DIGITAL_IO_SetOutputHigh(&DIGITAL_IO_DALI);
			break;
		}
		case MANCHESTER_TX_BIT1:	
		{						
			if (tx_data & tx_data_mask)
			{
				DIGITAL_IO_SetOutputLow(&DIGITAL_IO_DALI);
			}
			else
			{
				DIGITAL_IO_SetOutputHigh(&DIGITAL_IO_DALI);
			}
			break;
		}
		case MANCHESTER_TX_BIT2:		
		{
			if (tx_data & tx_data_mask)
			{
				DIGITAL_IO_SetOutputHigh(&DIGITAL_IO_DALI);
			}
			else
			{
				DIGITAL_IO_SetOutputLow(&DIGITAL_IO_DALI);
			}
	
			// shift mask to send next bit
			tx_data_mask >>= 1;
			break;
		}
		case MANCHESTER_TX_STOP1:									
		case MANCHESTER_TX_STOP2:									
		case MANCHESTER_TX_STOP3:									
		case MANCHESTER_TX_END:	
		{			
			DIGITAL_IO_SetOutputHigh(&DIGITAL_IO_DALI);
			stop_tx();
			tx_finished_cb();
			break;
		}
		case MANCHESTER_TX_IDLE:
		{			
			DIGITAL_IO_SetOutputHigh(&DIGITAL_IO_DALI);									
			break;
		}
		case MANCHESTER_TX_ERROR:
			stop_tx();			
			DIGITAL_IO_SetOutputHigh(&DIGITAL_IO_DALI);			
			tx_error_cb();
			break;
		default:
			break;
	}	
}

volatile static enum MANCHESTER_RX_STATE rx_transitions[9][3][3] =
{					/*SHORT_PULSE																																				LONG_PULSE																																				OTHER_PULE																											*/
					/*bitpos=0										bitpos=1 									bitpos>1										bitpos=0										bitpos=1 									bitpos>1										bitpos=0										bitpos=1 									bitpos>1	*/
/*IDLE*/	{ {MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_START_SYNC}, 	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR, 			MANCHESTER_RX_START_SYNC}, 	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,	 		MANCHESTER_RX_START_SYNC}},
/*SS*/		{ {MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_MIDDLE_SYNC},	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR},				{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}},
/*MS*/		{	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_START_ONE},		{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_MIDDLE_ZERO},	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}},
/*S1*/		{	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_STOP,				MANCHESTER_RX_MIDDLE_ONE},	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR},				{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}},
/*M1*/		{	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_START_ONE,	MANCHESTER_RX_START_ONE},		{MANCHESTER_RX_ERROR,				MANCHESTER_RX_MIDDLE_ZERO,MANCHESTER_RX_MIDDLE_ZERO},	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}},
/*S0*/		{	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_MIDDLE_ZERO,MANCHESTER_RX_MIDDLE_ZERO},	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR},				{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}},
/*M0*/		{	{MANCHESTER_RX_STOP,				MANCHESTER_RX_START_ZERO,	MANCHESTER_RX_START_ZERO},	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_STOP,				MANCHESTER_RX_MIDDLE_ONE},	{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}},
/*STOP*/  { {MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR},				{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR},				{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}},
/*ERROR*/ { {MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR},				{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR},				{MANCHESTER_RX_ERROR,				MANCHESTER_RX_ERROR,			MANCHESTER_RX_ERROR}}
};

void manchester_rx_interruptHandler(void)
{
	XMC_CCU4_SLICE_ClearEvent(CCU43_CC42, XMC_CCU4_SLICE_IRQ_ID_EVENT0);	
	uint32_t rx_pulsewidth = XMC_CCU4_SLICE_GetCaptureRegisterValue(CCU43_CC42, 3) & CCU4_CC4_CV_CAPTV_Msk;
		
#if ENABLE_DEBUG
	putDebugUInt32("(%i)", rx_pulsewidth);
	putDebugUInt32("(%i)", rx_bit_position);
	putDebugUInt32("RX:%i->", rx_state);
#endif
	
	rx_state = rx_transitions[rx_state][pulsetype(rx_pulsewidth)][(rx_bit_position > 1 ? 2 : rx_bit_position)];
	
#if ENABLE_DEBUG
	putDebugUInt32("%i, ", rx_state);
#endif
	
	switch (rx_state)
	{
		case MANCHESTER_RX_START_SYNC:
		{
			rx_start_cb();
			break;
		}
		case MANCHESTER_RX_MIDDLE_ONE: 
		{
			rx_bit_position--;
			rx_data |= (1<<rx_bit_position);			
			break;
		}
		case MANCHESTER_RX_MIDDLE_ZERO:
		{
			rx_bit_position--;
			rx_data |= (0<<rx_bit_position);			
			break;
		}
		case MANCHESTER_RX_STOP:
		{
			uint32_t time_interval = 0;
			if (rx_bit_position == 1)
			{				
				rx_data |= (1<<(rx_bit_position-1));	
				time_interval = TE_to_us(5)*100; //5TE * 200 (+10% tolerance)
			} 
			else
			{
				time_interval = TE_to_us(4)*100; //4TE * 200 (+10% tolerance)
			}
				
			TIMER_Stop(&MANCHESTER_RX_STOPBITS_TIMER);
			TIMER_SetTimeInterval(&MANCHESTER_RX_STOPBITS_TIMER, time_interval);
			TIMER_Start(&MANCHESTER_RX_STOPBITS_TIMER);
			break;
		}
		case MANCHESTER_RX_ERROR:
		{			
			stop_rx();
			rx_error_cb();
			break;
		}
		default: break;
	}	
}

void manchester_rx_stopbits_interruptHandler()
{	
	stop_rx();
	TIMER_Stop(&MANCHESTER_RX_STOPBITS_TIMER);	
	
#if ENABLE_DEBUG
	putDebugUInt32("RX:%i->", rx_state);
#endif
	
	//rx_state = MANCHESTER_RX_IDLE;			

#if ENABLE_DEBUG
	putDebugUInt32("%i\r\n", rx_state);
#endif
	
	rx_finished_cb(rx_data);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void manchester_initialize(void)
{	
	stop_tx();
	stop_rx();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MANCHESTER TX
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void initialize_tx(uint16_t data, callback_void finished, callback_void error)
{
	tx_finished_cb = finished;
	tx_error_cb = error;
	tx_data = data;
	tx_data_mask = 1<<15;	
	tx_state = MANCHESTER_TX_IDLE;
}

void start_tx(void)
{			
	tx_state = tx_transitions[tx_state][tx_data_mask == 0 ? 0 : 1];	
	
	TIMER_Stop(&MANCHESTER_TX_TIMER);
	TIMER_SetTimeInterval(&MANCHESTER_TX_TIMER, 41666); //TE = 416,66 �s	
	DIGITAL_IO_SetOutputLow(&DIGITAL_IO_DALI);
	TIMER_Start(&MANCHESTER_TX_TIMER);
}

void stop_tx(void)
{
	if (TIMER_GetTimerStatus(&MANCHESTER_TX_TIMER))
	{
		TIMER_Stop(&MANCHESTER_TX_TIMER);		
		tx_state = MANCHESTER_TX_IDLE;		
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MANCHESTER RX
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void initialize_rx(callback_void start, callback_uint8 finished, callback_void error)
{
	rx_start_cb = start;
	rx_finished_cb = finished;
	rx_error_cb = error;
	rx_bit_position = 8;
	rx_data = 0;
	rx_state = MANCHESTER_RX_IDLE;	
}

void start_rx(void)
{
	if (!XMC_CCU4_SLICE_IsTimerRunning(CCU43_CC42))
	{
		rx_state = MANCHESTER_RX_IDLE;
		XMC_CCU4_SLICE_EnableEvent(CCU43_CC42, XMC_CCU4_SLICE_IRQ_ID_EVENT0);
		XMC_CCU4_SLICE_StartTimer((XMC_CCU4_SLICE_t*)CCU43_CC42);
	}
}

void stop_rx(void)
{	
	if (XMC_CCU4_SLICE_IsTimerRunning(CCU43_CC42))
	{
		XMC_CCU4_SLICE_DisableEvent(CCU43_CC42, XMC_CCU4_SLICE_IRQ_ID_EVENT0);
		XMC_CCU4_SLICE_StopClearTimer((XMC_CCU4_SLICE_t*)CCU43_CC42);
		rx_state = MANCHESTER_RX_IDLE;
	}
}
