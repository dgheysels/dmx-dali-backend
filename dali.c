#include <stdlib.h>
#include <stdint.h>
#include <xmc4800.h>

#include "os_port.h"
#include "debug.h"
#include "my_debug.h"
#include "DAVE.h"

#include "manchester.h"
#include "dali.h"

//#define TE_to_us(te,margin_pct) (te*((((margin_pct*100)*416.66)+41666)/(margin_pct*100)))//((te*(417*(100+margin_pct)))/100)  /*�s/TE*/
#define TE_to_us(te) (te*416.66)
#define DALI_FINISHED_EVENT 0x02

enum DALI_STATE 
{
	DALI_SEND_READY, DALI_SENDING,
	DALI_BLOCK,
	DALI_RECEIVE_READY, DALI_RECEIVING, DALI_RECEIVED
}; 

volatile enum DALI_STATE dali_state;
	
static void tx_finished(void);
static void tx_error(void);
static void rx_started(void);
static void rx_finished(uint8_t f);
static void rx_error(void);

volatile uint16_t dali_txdata;
volatile int			dali_expectanswer;
volatile uint8_t dali_rxdata;

	
OsTask* dali_statemachine_task;
	
extern osMailQId			dali_recvPool_id;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// INTERRUPT HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
volatile static enum DALI_STATE dali_transitions[6] =
{
/*SND-RDY*/		DALI_SENDING,
/*SENDING*/		DALI_BLOCK,
/*BLOCK*/			DALI_RECEIVE_READY,
/*RCV-RDY*/		DALI_SEND_READY,// timeout
/*RCV-RDY*/		/*RECEIVING*/		// this transition is done via callback	'rx_started'
/*RECEIVING*/	DALI_RECEIVED,
/*RECEIVED*/	DALI_SEND_READY	
};
	
void dali_interruptHandler(void)
{			
#if ENABLE_DEBUG
	putDebugUInt32("D:%i->", dali_state);
#endif
	
	dali_state = dali_transitions[dali_state];
	
#if ENABLE_DEBUG
	putDebugUInt32("%i, ", dali_state);
#endif
	
	switch (dali_state)
	{
		case DALI_SEND_READY:	
		{			
			TIMER_Stop(&DALI_TIMER);		
			stop_rx();
			dali_result* daliresult = (dali_result*)osMailCAlloc(dali_recvPool_id, 0);
			daliresult->status = ((dali_expectanswer && (dali_rxdata == 0)) ? DALI_TIMEOUT : DALI_OK);
			daliresult->bframe.raw = dali_rxdata;					
			osMailPut(dali_recvPool_id, daliresult);	
			break;	
		}			
		case DALI_BLOCK:
		{			
			stop_tx();
			TIMER_Stop(&DALI_TIMER);
			TIMER_SetTimeInterval(&DALI_TIMER, TE_to_us(7)*100);
			initialize_rx(rx_started, rx_finished, rx_error);
			TIMER_Start(&DALI_TIMER);
			break;
		}
		case DALI_RECEIVE_READY:	
		{			
			TIMER_Stop(&DALI_TIMER);
			TIMER_SetTimeInterval(&DALI_TIMER, TE_to_us(15)*100);
			start_rx();
			TIMER_Start(&DALI_TIMER);
			break;
		}
		case DALI_RECEIVED:
		{
			TIMER_Stop(&DALI_TIMER);
			stop_rx();
			TIMER_SetTimeInterval(&DALI_TIMER, TE_to_us(22)*100);			
			TIMER_Start(&DALI_TIMER);
			break;
		}
		default:
			break;
	}	
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void dali_initialize()
{			
	dali_state = DALI_SEND_READY;
	manchester_initialize();	
}

void dali_send(uint16_t data, int expect_answer)
{		
	dali_expectanswer = expect_answer;
	dali_rxdata = 0;

	while (dali_state != DALI_SEND_READY);
	
#if ENABLE_DEBUG
	putDebugUInt32("D:%i->", dali_state);
#endif
	
	dali_state = dali_transitions[dali_state];
	
#if ENABLE_DEBUG
	putDebugUInt32("%i, ", dali_state);
#endif
	
	TIMER_Stop(&DALI_TIMER);
	TIMER_SetTimeInterval(&DALI_TIMER, TE_to_us(38)*100);
	initialize_tx(data, tx_finished, tx_error);
	TIMER_Start(&DALI_TIMER);
	start_tx();
	
	// <-- back to dali_handler task, waiting for DALI_RESULT in RECV_POOL
}

static void tx_finished(void)
{	
}

static void tx_error(void)
{	
	TIMER_Stop(&DALI_TIMER);
	dali_state = DALI_SEND_READY;	
	dali_rxdata = 0;
	dali_result* daliresult = (dali_result*)osMailCAlloc(dali_recvPool_id, 0);
	daliresult->status = DALI_TXERROR;
	daliresult->bframe.raw = dali_rxdata;
	osMailPut(dali_recvPool_id, daliresult);			
}

static void rx_started(void)
{	
#if ENABLE_DEBUG	
	putDebugUInt32("D:%i->", dali_state);
#endif
	
	TIMER_Stop(&DALI_TIMER);
	dali_state = DALI_RECEIVING;	
	
#if ENABLE_DEBUG
	putDebugUInt32("%i\r\n", dali_state);
#endif
	
	TIMER_SetTimeInterval(&DALI_TIMER, TE_to_us(30)*100);  /* WHY '28' instead of '22' ??? */
	TIMER_Start(&DALI_TIMER);	
}

static void rx_finished(uint8_t response)
{	
	dali_rxdata = response;
}

static void rx_error()
{
#if ENABLE_DEBUG
	putDebugString("RX:*ERR*, ", "");
#endif
	//TIMER_Stop(&DALI_TIMER);
	//dali_state = DALI_SEND_READY;	
	/*dali_result* daliresult = (dali_result*)osMailCAlloc(dali_recvPool_id, 0);
	daliresult->status = DALI_RXERROR;
	daliresult->bframe.raw = 0;
	osMailPut(dali_recvPool_id, daliresult);	
*/	
}


