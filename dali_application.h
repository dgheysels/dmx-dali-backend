#include "os_port.h"

#define DALI_ADDRESSING_EVENT 0x03

enum ACTION {ACTION_COMMAND, ACTION_DIRECTPWR};

typedef struct 
{
	uint8_t address;
	uint8_t data;
	enum ACTION  action;
} mqttIn_DALI_Data;

typedef struct 
{
	uint8_t result;
	int			error;
} mqttOut_DALI_Data;

void dali_initialize_application(OsTask* task);
