#include "core\net.h"
#include "drivers\xmc4800_eth.h"
#include "drivers\ksz8081.h"

#include "network.h"

error_t network_initialize()
{
	error_t error = NO_ERROR;
	NetInterface *interface;
	MacAddr macAddr;
	Ipv4Addr ipv4Addr;
	
	// initialise TCPIP stack
	error = netInit();
	if (error) return error;
	
	// configure first ethernet interface (eth0)
	interface = &netInterface[0];
	netSetInterfaceName(interface, "eth0");
	netSetHostname(interface, "xmc4800");
	netSetDriver(interface, &xmc4800EthDriver);
	netSetPhyDriver(interface, &ksz8081PhyDriver);
	macStringToAddr(MAC_ADDR, &macAddr);
	netSetMacAddr(interface, &macAddr);
	
	// initialise network interface
	error = netConfigInterface(interface);
	if (error) return error;
	
	error = nbnsInit(interface);
	if (error) return error;
	
	// configure IP		
	ipv4StringToAddr(IPV4_HOST_ADDR, &ipv4Addr);
	ipv4SetHostAddr(interface, ipv4Addr);
	ipv4StringToAddr(IPV4_SUBNET_MASK, &ipv4Addr);
	ipv4SetSubnetMask(interface, ipv4Addr);
	ipv4StringToAddr(IPV4_DEFAULT_GATEWAY, &ipv4Addr);
	ipv4SetDefaultGateway(interface, ipv4Addr);
	//ipv4StringToAddr(IPV4_PRIMARY_DNS, &ipv4Addr);
	//ipv4SetDnsServer(interface, 0, ipv4Addr);		 
	
	return error;
}

error_t mqtt_initialize(MqttClientContext* mqtt_ctx)
{
	error_t error = NO_ERROR;
	mqtt_ctx->keepAlive = 1;
	mqttClientInit(mqtt_ctx);	
	mqttClientSetIdentifier(mqtt_ctx, "mqtt_xmc4800");
	
	return error;
}

error_t mqtt_registerPublishCallback(MqttClientContext* mqtt_ctx, MqttClientPublishCallback f)
{
	return mqttClientRegisterPublishCallback(mqtt_ctx, f);
}

error_t mqtt_connect(MqttClientContext* mqtt_ctx)
{
	error_t error = NO_ERROR;
	
	IpAddr mqtt_server;
	ipStringToAddr(MQTT_IPV4_SRV_ADDR, &mqtt_server);

	error = mqttClientOpen(mqtt_ctx, &mqtt_server, MQTT_IPV4_SRV_PORT, MQTT_URI);
	if (error) return error;
	error = mqttClientConnect(mqtt_ctx, 1);
	if (error) return error;
	
	return error;
}

error_t mqtt_subscribe(MqttClientContext* mqtt_ctx, char_t* topic)
{
	error_t error = NO_ERROR;
	
	error = mqttClientSubscribe(mqtt_ctx, topic, MQTT_QOS_LEVEL_1);
	if (error) return error;
	
	return error;
}
