#include <stdlib.h>
#include <stdint.h>
#include <xmc4800.h>

#include "os_port.h"
#include "debug.h"
#include "DAVE.h"

#include "dmx.h"
#include "dmx_application.h"

static void fill_channels(uint8_t* channels, int from, int n);
static void dmx_statemachine(void*);

enum DMX512_STATE {DMX_IDLE, DMX_BREAK, DMX_MAB, DMX_SLOTS, DMX_ERROR};
volatile enum DMX512_STATE dmx512_state;

volatile uint8_t  frame_data[513] = {0};
volatile uint16_t current_slot = 0;

extern OsTask* dmx_send_task;

static enum DMX512_STATE dmx_transitions[4][2] =
{
/*DMX_IDLE*/	{	DMX_BREAK,	DMX_ERROR},
/*DMX_BREAK*/	{	DMX_MAB,		DMX_ERROR},
/*DMX_MAB*/		{	DMX_SLOTS,	DMX_ERROR},
/*DMX_SLOTS*/	{	DMX_SLOTS,	DMX_IDLE},
/*DMX_ERROR*/
};

void dmx512_interruptHandler(void)
{
	dmx512_state = dmx_transitions[dmx512_state][current_slot < 513 ? 0 : 1];
	
	switch(dmx512_state)
	{
		case DMX_IDLE:
			TIMER_Stop(&TIMER_0);			
			break;
		case DMX_BREAK:
			/* manual - dmx_send_frame */
			break;
		case DMX_MAB:
		{
			TIMER_Stop(&TIMER_0);
			TIMER_SetTimeInterval(&TIMER_0, 1200); // MAB = 12us
			DIGITAL_IO_SetOutputHigh(&DIGITAL_IO_0);	
			TIMER_Start(&TIMER_0);	
			break;
		}
		case DMX_SLOTS:
		{
			TIMER_Stop(&TIMER_0);
			TIMER_SetTimeInterval(&TIMER_0, 4400); // SLOT = 44us
			XMC_GPIO_Init(UART_1.config->tx_pin_config->port, UART_1.config->tx_pin_config->pin, UART_1.config->tx_pin_config->config);
			UART_Transmit(&UART_1, &frame_data[current_slot], sizeof(frame_data[current_slot]));	
			TIMER_Start(&TIMER_0);	
			break;
		}
		case DMX_ERROR:
		{
			TIMER_Stop(&TIMER_0);			
			break;
		}
		default: break;		
	}	
}

void dmx512_slotTransmittedInterruptHandler(void)
{
	UART_ClearFlag(&UART_1, XMC_UART_CH_STATUS_FLAG_TRANSMITTER_FRAME_FINISHED);
	current_slot++;	
}

void dmx_initialize(void)
{	
	dmx512_state = DMX_IDLE;
	current_slot = 0;
}

void dmx_send_frame(uint8_t* channels, int first, int n)
{	
	TIMER_Stop(&TIMER_0);
	TIMER_SetTimeInterval(&TIMER_0, 17600); // BREAK = 176us
	DIGITAL_IO_Init(&DIGITAL_IO_0);
	DIGITAL_IO_SetOutputLow(&DIGITAL_IO_0);		
	
	current_slot = 0;
	fill_channels(channels, first, n);
	free(channels);
	
	dmx512_state = dmx_transitions[DMX_IDLE][0];	
	TIMER_Start(&TIMER_0);	
}

void fill_channels(uint8_t* channels, int first, int n)
{
	for (int i=0; i<n; i++)
	{
		int channel = first+i;
		if (channel > 0 && channel < 513)
		{
			frame_data[channel] = channels[i];
		}		
	}
}