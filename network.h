#include "netbios\nbns_client.h"
#include "mqtt\mqtt_client.h"

#define MAC_ADDR "00-AB-CD-EF-47-00"
#define IPV4_HOST_ADDR "10.0.0.3"
#define IPV4_SUBNET_MASK "255.0.0.0"
#define IPV4_DEFAULT_GATEWAY "10.0.0.2"
#define IPV4_PRIMARY_DNS "10.0.0.2"
	
#define MQTT_IPV4_SRV_ADDR "10.0.0.2"
#define MQTT_IPV4_SRV_PORT 1883
#define MQTT_URI					 "mqtt://10.0.0.2"

error_t network_initialize(void);
error_t mqtt_initialize(MqttClientContext*);
error_t mqtt_registerPublishCallback(MqttClientContext*, MqttClientPublishCallback);
error_t mqtt_connect(MqttClientContext*);
error_t mqtt_subscribe(MqttClientContext*, char_t* topic);
