#include <stdint.h>

typedef struct
{
	int first_channel;
	int number_of_channels;
	uint8_t* data;
} dmx_frame;

void dmx_initialize(void);
void dmx_send_frame(uint8_t*, int, int);
