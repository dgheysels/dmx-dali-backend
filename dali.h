#include <stdint.h>

enum DALI_STATUS
{
	DALI_OK, DALI_TIMEOUT, DALI_TXERROR, DALI_RXERROR
};

typedef struct 
{	
	uint8_t raw;
} dali_backwardframe;

typedef struct
{
	dali_backwardframe bframe;
	enum DALI_STATUS status;
} dali_result;

typedef void (* volatile callback_uint8)(uint8_t);

void dali_initialize(void);
void dali_send(uint16_t, int);
