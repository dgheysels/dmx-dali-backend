#include <stdint.h>
#include <stdlib.h>

#include "os_port.h"

#include "debug.h"
#include "my_debug.h"

#include "DAVE.h"

#include "dali.h"
#include "dali_commands.h"
#include "dali_application.h"

volatile uint8_t next_short_address = 0;

static void dali_initAddressingProcess(void*);
static void dali_executeCommand(void*);
static void dali_handleCommand(void*);

static void search_slaves(uint32_t, int8_t);
static uint8_t search_slave_with_address(uint32_t);
static void assign_shortaddress_and_withdraw(uint32_t);
static dali_result send_dalicommand(uint8_t, uint8_t, uint8_t);

osMutexDef (dali_mutex);
osMutexId  dali_mutex_id;

osMailQDef (dali_sendPool, 3, dalicommand_t);
osMailQId	 dali_sendPool_id;
osMailQDef (dali_recvPool, 3, dali_result);
osMailQId	 dali_recvPool_id;

extern osMailQId mqttIn_DALI_id;
extern osMailQId mqttOut_DALI_id;

OsTask* dali_executeCommand_task;
OsTask* dali_initAddressingProcess_task;
OsTask* dali_commandhandler_task;

osEvent event_dali_responsereceived;

void dali_initialize_application(OsTask* task)
{
	dali_mutex_id = osMutexCreate(osMutex(dali_mutex));
	
	dali_sendPool_id = osMailCreate(osMailQ(dali_sendPool), NULL);
	dali_recvPool_id = osMailCreate(osMailQ(dali_recvPool), NULL);
		
	dali_executeCommand_task 				= osCreateTask("DALI EXECUTE COMMAND", dali_executeCommand, NULL, 50, OS_TASK_PRIORITY_NORMAL);
	dali_initAddressingProcess_task = osCreateTask("DALI INIT ADDRESSING PROCESS", dali_initAddressingProcess, NULL, 50, OS_TASK_PRIORITY_NORMAL);
	dali_commandhandler_task 				= osCreateTask("DALI COMMANDHANDLER", dali_handleCommand, NULL, 50, OS_TASK_PRIORITY_NORMAL);
	
	dali_initialize();	
}

static void dali_initAddressingProcess(void* args)
{
	while (1)
	{
		osEvent daliAddressing_signal = osSignalWait(DALI_ADDRESSING_EVENT, osWaitForever);
		if (daliAddressing_signal.status == osEventSignal)
		{
			osStatus status = osMutexWait(dali_mutex_id, 1000);
			if (status != osOK)
			{
				TRACE_INFO("[dali_initAddressingProcess] cannot start addressing procedure");
				mqttOut_DALI_Data* out = (mqttOut_DALI_Data*)osMailCAlloc(mqttOut_DALI_id, osWaitForever);
				out->error = 1;
				osMailPut(mqttOut_DALI_id, out);
			}
			else
			{
				TRACE_INFO("[dali_initAddressingProcess] addressing procedure initiated...\n\r");
						
				next_short_address = 0;
				
				// eerst alle luminaire uitzetten ?
				// ...				
											
				// INITIALISE
				send_dalicommand(0xA5, 0x00, 1);									
				// RANDOMISE
				send_dalicommand(0xA7, 0x00, 1);
				
				
				/*** print slaves ****/
#if ENABLE_DEBUG_COMMISSIONING
				osDelay(2000);
				send_dalicommand(0xFF, 0x09, 0);	
				osDelay(2000);
#endif
				
				// BINARY SEARCH 24-bit addresses for the luminaire with the lowest address
				search_slaves(1<<23, 23);
	
				// TERMINATE
				send_dalicommand(0xA1, 0x00, 0);				
			
				/*** print slaves ****/
#if ENABLE_DEBUG_COMMISSIONING
				osDelay(2000);
				send_dalicommand(0xFF, 0x09, 0);	
				osDelay(2000);
#endif

				mqttOut_DALI_Data* out = (mqttOut_DALI_Data*)osMailCAlloc(mqttOut_DALI_id, osWaitForever);
				out->result = next_short_address - 1;
				osMailPut(mqttOut_DALI_id, out);

				osMutexRelease(dali_mutex_id);
				
				// in user-application :
				//		assign short addresses to 'luminaire-widgets'
				//		  option to test luminaire, to see which luminaire is assigned to which short-address
				//			option to give a meaningful description to luminaire
				
			}
		}
	}
}

static void search_slaves(uint32_t root, int8_t h)
{		
	if (h <= 0)
	{
		if (root == 0x000001)
		{
			search_slaves(0, h-1);	
		}
		if (search_slave_with_address(root) == 0xFF)
		{			
			assign_shortaddress_and_withdraw(root);
		}
	}
	else
	{
		if (search_slave_with_address(root) == 0xFF)
		{
			search_slaves(root^(3<<(h-1)), h-1); //lower branch
			if (search_slave_with_address(root) == 0xFF)
			{					
				assign_shortaddress_and_withdraw(root); 
			}
		}
		search_slaves(root^(1<<(h-1)), h-1);   //upper branch
	}	
}

static uint8_t search_slave_with_address(uint32_t address)
{
	send_dalicommand(0xB1, (address & 0x00FF0000)>>16, 0);
	send_dalicommand(0xB3, (address & 0x0000FF00)>>8, 0);
	send_dalicommand(0xB5, address & 0x000000FF, 0);

	dali_result result;
	result = send_dalicommand(0xA9, 0x00, 0);
	
	return result.bframe.raw;
}

static void assign_shortaddress_and_withdraw(uint32_t address)
{	
	send_dalicommand(0xB7, (next_short_address<<1) | 0x01, 0);
	send_dalicommand(0xAB, 0x00, 0);
	
	next_short_address++;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void dali_executeCommand(void* args)
{
	while (1)
	{
		osEvent mqttIn_mail = osMailGet(mqttIn_DALI_id, osWaitForever);
		if (mqttIn_mail.status == osEventMail)
		{
			mqttIn_DALI_Data* in = (mqttIn_DALI_Data*)mqttIn_mail.value.p;
			
			osStatus status = osMutexWait(dali_mutex_id, 0);
			if (status != osOK)
			{
				osMailFree(mqttIn_DALI_id, in);
				mqttOut_DALI_Data* out = (mqttOut_DALI_Data*)osMailCAlloc(mqttOut_DALI_id, osWaitForever);
				out->result = 2; // could not send command, dali commissioning in process...
				osMailPut(mqttOut_DALI_id, out);
			}
			else
			{				
				// calculate short address / group address
				// (broadcast address = 255)
				uint8_t address = in->address;
				uint8_t group   = 0;
				if (address >= 64 && address <= 79)
				{		
					group = 1;
					address -= 64;					
				}
	
				dali_result result = send_dalicommand( ((group<<7)|(address<<1)|(in->action == ACTION_COMMAND)), in->data, 0/*twice ??*/);
																
				osMailFree(mqttIn_DALI_id, in);
				
#if ENABLE_DEBUG
				printDebugStringToUART();
#endif
				
				mqttOut_DALI_Data* out = (mqttOut_DALI_Data*)osMailCAlloc(mqttOut_DALI_id, osWaitForever);							
				switch (result.status)
				{
					case DALI_OK:
					{
						out->result = result.bframe.raw; // ok ?
						out->error = 0;
						break;
					}
					case DALI_TIMEOUT:
						out->result = 0;
						out->error = 0;
					case DALI_TXERROR:
					case DALI_RXERROR:
						out->result = 0;
						out->error = 1;
						break;
					default: // should not happen
						out->error = -1;
						break;
				}
				
					
				osMailPut(mqttOut_DALI_id, out);
				
							
				osMutexRelease(dali_mutex_id);
				
			}
		}
	}
}

static dali_result send_dalicommand(uint8_t addresspart, uint8_t datapart, uint8_t twice)
{
	dalicommand_t* command  = (dalicommand_t*)osMailCAlloc(dali_sendPool_id, osWaitForever);
	command->framedata.addresspart = addresspart;
	command->framedata.datapart = datapart;
	
	osMailPut(dali_sendPool_id, command);
	osEvent command_reply = osMailGet(dali_recvPool_id, osWaitForever);

#if ENABLE_DEBUG
	printDebugStringToUART();
#endif
	
	
	if (command_reply.status == osEventMail) 
	{	
		dali_result* r = (dali_result*) command_reply.value.p;
		if (twice)
		{
			osMailFree(dali_recvPool_id, r);		
			return send_dalicommand(addresspart, datapart, 0);
		}
		else
		{
				
			dali_result result;
			result.status = r->status;
			result.bframe.raw = r->bframe.raw;
			osMailFree(dali_recvPool_id, r);			
		
			return result;
		}
	}
	else
	{
		dali_result result;
		result.status = DALI_TIMEOUT;
		return result;
	}
}


/**
PUT IN ANOTHER FILE
*/
void dali_handleCommand(void* args)
{
	while (1)
	{		
		osEvent event = osMailGet(dali_sendPool_id, osWaitForever);
		if (event.status == osEventMail)
		{
			dalicommand_t* command = (dalicommand_t*)event.value.p;
			uint16_t raw = command->raw;
			osMailFree(dali_sendPool_id, command);			
			
			dali_send(raw, 1/*command->info.expectAnswer*/);			
		}
	}
}
