#define ENABLE_DEBUG 0
#define ENABLE_DEBUG_COMMISSIONING 1

void putDebugString(char* pattern, char* value);
void putDebugUInt32(char* pattern, uint32_t value);
void putDebugPtr(char* pattern, void* value);
void printDebugStringToUART(void);
void clearDebugData(void);
