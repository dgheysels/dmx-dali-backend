/**
 * @file net_config.h
 * @brief CycloneTCP configuration file
 *
 * @section License
 *
 * Copyright (C) 2010-2016 Oryx Embedded SARL. All rights reserved.
 *
 * This file is part of CycloneTCP Open.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @author Oryx Embedded SARL (www.oryx-embedded.com)
 * @version 1.7.4
 **/

#ifndef _NET_CONFIG_H
#define _NET_CONFIG_H

//Dependencies
#include "RTE_Components.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***

// <o>Number of network adapters
// <i>Number of network adapters
// <i>Default: 1
// <1-16>
#define NET_INTERFACE_COUNT 1

// <h>Trace level

// <o>Memory Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define MEM_TRACE_LEVEL 3

// <o>NIC Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define NIC_TRACE_LEVEL 3

// <o>Ethernet Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define ETH_TRACE_LEVEL 3

// <o>ARP Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define ARP_TRACE_LEVEL 3

// <o>IP Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define IP_TRACE_LEVEL 3

// <o>IPv4 Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define IPV4_TRACE_LEVEL 3

// <o>IPv6 Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define IPV6_TRACE_LEVEL 3

// <o>ICMP Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define ICMP_TRACE_LEVEL 3

// <o>IGMP Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define IGMP_TRACE_LEVEL 3

// <o>ICMPv6 Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define ICMPV6_TRACE_LEVEL 3

// <o>MLD Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define MLD_TRACE_LEVEL 3

// <o>NDP Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define NDP_TRACE_LEVEL 3

// <o>UDP Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define UDP_TRACE_LEVEL 3

// <o>TCP Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define TCP_TRACE_LEVEL 3

// <o>Socket Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define SOCKET_TRACE_LEVEL 3

// <o>Raw socket Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define RAW_SOCKET_TRACE_LEVEL 3

// <o>BSD socket Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define BSD_SOCKET_TRACE_LEVEL 3

// <o>WebSocket Trace level
// <i>Set the desired debugging level
// <i>Default: Error
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define WEB_SOCKET_TRACE_LEVEL 3

// <o>Auto-IP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define AUTO_IP_TRACE_LEVEL 3

// <o>SLAAC Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define SLAAC_TRACE_LEVEL 3

// <o>DHCP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define DHCP_TRACE_LEVEL 3

// <o>DHCPv6 Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define DHCPV6_TRACE_LEVEL 3

// <o>DNS Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define DNS_TRACE_LEVEL 3

// <o>mDNS Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define MDNS_TRACE_LEVEL 3

// <o>NBNS Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define NBNS_TRACE_LEVEL 3

// <o>LLMNR Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define LLMNR_TRACE_LEVEL 3

// <o>FTP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define FTP_TRACE_LEVEL 3

// <o>HTTP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define HTTP_TRACE_LEVEL 3

// <o>MQTT Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define MQTT_TRACE_LEVEL 3

// <o>SMTP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define SMTP_TRACE_LEVEL 3

// <o>SNMP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define SNMP_TRACE_LEVEL 3

// <o>SNTP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define SNTP_TRACE_LEVEL 3

// <o>TFTP Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define TFTP_TRACE_LEVEL 3

// <o>Std Services Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define STD_SERVICES_TRACE_LEVEL 3

// </h>
// <h>Ethernet

// <o>Size of the multicast MAC filter
// <i>Maximum number of entries in the multicast MAC filter
// <i>Default: 12
// <1-1024>
#define MAC_MULTICAST_FILTER_SIZE 12

// </h>
// <h>IPv4

// <o>Size of the IPv4 multicast filter
// <i>Maximum number of entries in the IPv4 multicast filter
// <i>Default: 4
// <1-1024>
#define IPV4_MULTICAST_FILTER_SIZE 4

// <q>IPv4 fragmentation support
// <i>Enable IPv4 fragmentation and reassembly support
// <i>Default: Enabled
#define IPV4_FRAG_SUPPORT 1

// <o>Maximum number of fragmented packets
// <i>Maximum number of fragmented packets the host will accept and hold in the reassembly queue simultaneously
// <i>Default: 4
// <1-1024>
#define IPV4_MAX_FRAG_DATAGRAMS 4

// <o>Maximum datagram size
// <i>Maximum datagram size the host will accept when reassembling fragments
// <i>Default: 8192
// <576-65536>
#define IPV4_MAX_FRAG_DATAGRAM_SIZE 8192

// <o>Size of ARP cache
// <i>Size of ARP cache
// <i>Default: 8
// <1-1024>
#define ARP_CACHE_SIZE 8

// <o>Maximum number of pending packets
// <i>Maximum number of packets waiting for address resolution to complete
// <i>Default: 2
// <1-1024>
#define ARP_MAX_PENDING_PACKETS 2

// </h>
// <h>IPv6

// <o>Size of the IPv6 multicast filter
// <i>Maximum number of entries in the IPv6 multicast filter
// <i>Default: 8
// <1-1024>
#define IPV6_MULTICAST_FILTER_SIZE 8

// <q>IPv6 fragmentation support
// <i>Enable IPv6 fragmentation and reassembly support
// <i>Default: Enabled
#define IPV6_FRAG_SUPPORT 1

// <o>Maximum number of fragmented packets
// <i>Maximum number of fragmented packets the host will accept and hold in the reassembly queue simultaneously
// <i>Default: 4
// <1-1024>
#define IPV6_MAX_FRAG_DATAGRAMS 4

// <o>Maximum datagram size
// <i>Maximum datagram size the host will accept when reassembling fragments
// <i>Default: 8192
// <576-65536>
#define IPV6_MAX_FRAG_DATAGRAM_SIZE 8192

// <o>Size of the Neighbor Cache size
// <i>Size of the Neighbor Cache size
// <i>Default: 8
// <1-1024>
#define NDP_NEIGHBOR_CACHE_SIZE 8

// <o>Size of the Destination Cache size
// <i>Size of the Destination Cache size
// <i>Default: 8
// <1-1024>
#define NDP_DEST_CACHE_SIZE 8

// <o>Maximum number of pending packets
// <i>Maximum number of packets waiting for address resolution to complete
// <i>Default: 2
// <1-1024>
#define NDP_MAX_PENDING_PACKETS 2

// </h>
// <h>TCP

// <o>Default buffer size for transmission
// <i>Default buffer size for transmission
// <i>Default: 2860
// <576-65536>
#define TCP_DEFAULT_TX_BUFFER_SIZE 2860

// <o>Default buffer size for reception
// <i>Default buffer size for reception
// <i>Default: 2860
// <576-65536>
#define TCP_DEFAULT_RX_BUFFER_SIZE 2860

// <o>Default SYN queue size for listening sockets
// <i>Default SYN queue size for listening sockets
// <i>Default: 4
// <1-1024>
#define TCP_DEFAULT_SYN_QUEUE_SIZE 4

// <o>Maximum number of retransmissions
// <i>Maximum number of retransmissions
// <i>Default: 5
// <1-1024>
#define TCP_MAX_RETRIES 5

// <q>SACK support
// <i>Enable selective acknowledgment support
// <i>Default: Disabled
#define TCP_SACK_SUPPORT 0

// </h>
// <h>UDP

// <o>Receive queue depth for connectionless sockets
// <i>Receive queue depth for connectionless sockets
// <i>Default: 4
// <1-1024>
#define UDP_RX_QUEUE_SIZE 4

// </h>
// <h>Socket

// <o>Receive queue depth for connectionless sockets
// <i>Receive queue depth for connectionless sockets
// <i>Default: 16
// <1-1024>
//Number of sockets that can be opened simultaneously
#define SOCKET_MAX_COUNT 16

// <q>BSD socket support
// <i>Enable BSD socket support
// <i>Default: Disabled
#define BSD_SOCKET_SUPPORT 0

// <q>Raw socket support
// <i>Enable raw socket support
// <i>Default: Disabled
#define RAW_SOCKET_SUPPORT 0

// </h>
// <h>HTTP Server

// <q>File system support
// <i>Enable file system support
// <i>Default: Disabled
#define HTTP_SERVER_FS_SUPPORT 0

// <q>SSI support
// <i>Enable Server Side Includes support
// <i>Default: Disabled
#define HTTP_SERVER_SSI_SUPPORT 0

// <q>Basic access authentication support
// <i>Enable basic access authentication support
// <i>Default: Disabled
#define HTTP_SERVER_BASIC_AUTH_SUPPORT 0

// <q>Digest access authentication support
// <i>Enable digest access authentication support
// <i>Default: Disabled
#define HTTP_SERVER_DIGEST_AUTH_SUPPORT 0

// </h>

//TCP support
#ifdef RTE_CYCLONE_TCP_TCP
   #define TCP_SUPPORT ENABLED
#else
   #define TCP_SUPPORT DISABLED
#endif

//UDP support
#ifdef RTE_CYCLONE_TCP_UDP
   #define UDP_SUPPORT ENABLED
#else
   #define UDP_SUPPORT DISABLED
#endif

//IPv4 support
#ifdef RTE_CYCLONE_TCP_IPV4
   #define IPV4_SUPPORT ENABLED
#else
   #define IPV4_SUPPORT DISABLED
#endif

//IGMP support
#ifdef RTE_CYCLONE_TCP_AUTO_IGMP
   #define IGMP_SUPPORT ENABLED
#else
   #define IGMP_SUPPORT DISABLED
#endif

//Auto-IP support
#ifdef RTE_CYCLONE_TCP_AUTO_IP
   #define AUTO_IP_SUPPORT ENABLED
#else
   #define AUTO_IP_SUPPORT DISABLED
#endif

//IPv6 support
#ifdef RTE_CYCLONE_TCP_IPV6
   #define IPV6_SUPPORT ENABLED
#else
   #define IPV6_SUPPORT DISABLED
#endif

//MLD support
#ifdef RTE_CYCLONE_TCP_MLD
   #define MLD_SUPPORT ENABLED
#else
   #define MLD_SUPPORT DISABLED
#endif

//SLAAC support
#ifdef RTE_CYCLONE_TCP_SLAAC
   #define SLAAC_SUPPORT ENABLED
#else
   #define SLAAC_SUPPORT DISABLED
#endif

//RA service support
#ifdef RTE_CYCLONE_TCP_ROUTER_ADV
   #define NDP_ROUTER_ADV_SUPPORT ENABLED
#else
   #define NDP_ROUTER_ADV_SUPPORT DISABLED
#endif

//DHCP client support
#ifdef RTE_CYCLONE_TCP_DHCP_CLIENT
   #define DHCP_CLIENT_SUPPORT ENABLED
#else
   #define DHCP_CLIENT_SUPPORT DISABLED
#endif

//DHCP server support
#ifdef RTE_CYCLONE_TCP_DHCP_SERVER
   #define DHCP_SERVER_SUPPORT ENABLED
#else
   #define DHCP_SERVER_SUPPORT DISABLED
#endif

//DHCPv6 client support
#ifdef RTE_CYCLONE_TCP_DHCPV6_CLIENT
   #define DHCPV6_CLIENT_SUPPORT ENABLED
#else
   #define DHCPV6_CLIENT_SUPPORT DISABLED
#endif

//DHCPv6 relay agent support
#ifdef RTE_CYCLONE_TCP_DHCPV6_RELAY
   #define DHCPV6_RELAY_SUPPORT ENABLED
#else
   #define DHCPV6_RELAY_SUPPORT DISABLED
#endif

//DNS client support
#ifdef RTE_CYCLONE_TCP_DNS_CLIENT
   #define DNS_CLIENT_SUPPORT ENABLED
#else
   #define DNS_CLIENT_SUPPORT DISABLED
#endif

//mDNS client support
#ifdef RTE_CYCLONE_TCP_MDNS_CLIENT
   #define MDNS_CLIENT_SUPPORT ENABLED
#else
   #define MDNS_CLIENT_SUPPORT DISABLED
#endif

//mDNS responder support
#ifdef RTE_CYCLONE_TCP_MDNS_RESPONDER
   #define MDNS_RESPONDER_SUPPORT ENABLED
#else
   #define MDNS_RESPONDER_SUPPORT DISABLED
#endif

//DNS-SD support
#ifdef RTE_CYCLONE_TCP_DNS_SD
   #define DNS_SD_SUPPORT ENABLED
#else
   #define DNS_SD_SUPPORT DISABLED
#endif

//NBNS client support
#ifdef RTE_CYCLONE_TCP_NBNS_CLIENT
   #define NBNS_CLIENT_SUPPORT ENABLED
#else
   #define NBNS_CLIENT_SUPPORT DISABLED
#endif

//NBNS responder support
#ifdef RTE_CYCLONE_TCP_NBNS_RESPONDER
   #define NBNS_RESPONDER_SUPPORT ENABLED
#else
   #define NBNS_RESPONDER_SUPPORT DISABLED
#endif

//FTP client support
#ifdef RTE_CYCLONE_TCP_FTP_CLIENT
   #define FTP_CLIENT_SUPPORT ENABLED
#else
   #define FTP_CLIENT_SUPPORT DISABLED
#endif

//FTP over SSL/TLS
#ifdef RTE_CYCLONE_TCP_FTPS_CLIENT
   #define FTP_CLIENT_TLS_SUPPORT ENABLED
#else
   #define FTP_CLIENT_TLS_SUPPORT DISABLED
#endif

//FTP server support
#ifdef RTE_CYCLONE_TCP_FTP_SERVER
   #define FTP_SERVER_SUPPORT ENABLED
#else
   #define FTP_SERVER_SUPPORT DISABLED
#endif

//HTTP server support
#ifdef RTE_CYCLONE_TCP_HTTP_SERVER
   #define HTTP_SERVER_SUPPORT ENABLED
#else
   #define HTTP_SERVER_SUPPORT DISABLED
#endif

//HTTP over SSL/TLS
#ifdef RTE_CYCLONE_TCP_HTTPS_SERVER
   #define HTTP_SERVER_TLS_SUPPORT ENABLED
#else
   #define HTTP_SERVER_TLS_SUPPORT DISABLED
#endif

//MQTT client support
#ifdef RTE_CYCLONE_TCP_MQTT_CLIENT
   #define MQTT_CLIENT_SUPPORT ENABLED
#else
   #define MQTT_CLIENT_SUPPORT DISABLED
#endif

//MQTT over SSL/TLS
#ifdef RTE_CYCLONE_TCP_MQTT_CLIENT_TLS
   #define MQTT_CLIENT_TLS_SUPPORT ENABLED
#else
   #define MQTT_CLIENT_TLS_SUPPORT DISABLED
#endif

//MQTT over WebSocket
#ifdef RTE_CYCLONE_TCP_MQTT_CLIENT_WS
   #define MQTT_CLIENT_WS_SUPPORT ENABLED
#else
   #define MQTT_CLIENT_WS_SUPPORT DISABLED
#endif

//SMTP client support
#ifdef RTE_CYCLONE_TCP_SMTP_CLIENT
   #define SMTP_CLIENT_SUPPORT ENABLED
#else
   #define SMTP_CLIENT_SUPPORT DISABLED
#endif

//SMTP over SSL/TLS
#ifdef RTE_CYCLONE_TCP_SMTPS_CLIENT
   #define SMTP_CLIENT_TLS_SUPPORT ENABLED
#else
   #define SMTP_CLIENT_TLS_SUPPORT DISABLED
#endif

//SNMP agent support
#ifdef RTE_CYCLONE_TCP_SNMP_AGENT
   #define SNMP_AGENT_SUPPORT ENABLED
#else
   #define SNMP_AGENT_SUPPORT DISABLED
#endif

//MIB-II module support
#ifdef RTE_CYCLONE_TCP_MIB2
   #define MIB2_SUPPORT ENABLED
#else
   #define MIB2_SUPPORT DISABLED
#endif

//SNTP client support
#ifdef RTE_CYCLONE_TCP_SNTP_CLIENT
   #define SNTP_CLIENT_SUPPORT ENABLED
#else
   #define SNTP_CLIENT_SUPPORT DISABLED
#endif

//TFTP server support
#ifdef RTE_CYCLONE_TCP_TFTP_SERVER
   #define TFTP_SERVER_SUPPORT ENABLED
#else
   #define TFTP_SERVER_SUPPORT DISABLED
#endif

//Icecast client support
#ifdef RTE_CYCLONE_TCP_ICECAST_CLIENT
   #define ICECAST_CLIENT_SUPPORT ENABLED
#else
   #define ICECAST_CLIENT_SUPPORT DISABLED
#endif

//WebSocket support
#ifdef RTE_CYCLONE_TCP_WEBSOCKET
   #define WEB_SOCKET_SUPPORT ENABLED
#else
   #define WEB_SOCKET_SUPPORT DISABLED
#endif

//Support for WebSocket connections over SSL/TLS
#ifdef RTE_CYCLONE_TCP_WEBSOCKET_TLS
   #define WEB_SOCKET_TLS_SUPPORT ENABLED
#else
   #define WEB_SOCKET_TLS_SUPPORT DISABLED
#endif

//PPP support
#ifdef RTE_CYCLONE_TCP_PPP
   #define PPP_SUPPORT ENABLED
#else
   #define PPP_SUPPORT DISABLED
#endif

//PAP authentication support
#ifdef RTE_CYCLONE_TCP_PAP
   #define PAP_SUPPORT ENABLED
#else
   #define PAP_SUPPORT DISABLED
#endif

//CHAP authentication support
#ifdef RTE_CYCLONE_TCP_CHAP
   #define CHAP_SUPPORT ENABLED
#else
   #define CHAP_SUPPORT DISABLED
#endif

#define USE_XMC4800_RELAX_ECAT_KIT

#endif
