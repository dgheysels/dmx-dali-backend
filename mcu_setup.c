#include "DAVE.h"
#include "mcu_setup.h"

static enum STATUS dmx_initialize_peripherals(void);
static enum STATUS dali_initialize_peripherals(void);
static void dali_initialize_captureTimer(void);

enum STATUS mcu_initialize(void)
{
	enum STATUS status = FAILURE;
	
	if (dmx_initialize_peripherals() == SUCCESS)
	{
		if (dali_initialize_peripherals() == SUCCESS)
		{
			status = SUCCESS;
		}
	}
	
	return status;
}

static enum STATUS dmx_initialize_peripherals(void)
{
	DAVE_STATUS_t status = DAVE_Init_DMX();	
	
	if (status == DAVE_STATUS_SUCCESS)
		return SUCCESS;
	else
		return FAILURE;		
}

static enum STATUS dali_initialize_peripherals(void)
{
	DAVE_STATUS_t status = DAVE_Init_DALI();	
	dali_initialize_captureTimer();
	
	if (status == DAVE_STATUS_SUCCESS)
		return SUCCESS;
	else
		return FAILURE;	
}

static void dali_initialize_captureTimer(void)
{
	// initialisation structures
	XMC_CCU4_SLICE_CAPTURE_CONFIG_t MANCHESTER_RX_TIMER_config =
	{
		.fifo_enable = false,
		.timer_clear_mode = XMC_CCU4_SLICE_TIMER_CLEAR_MODE_ALWAYS,
		.same_event = true,
		.ignore_full_flag = true,
		.prescaler_mode = XMC_CCU4_SLICE_PRESCALER_MODE_NORMAL,
		// 2.250 Mhz
		// 0.444 �s / tick
		// 416 �s = 945 ticks
		.prescaler_initval = XMC_CCU4_SLICE_PRESCALER_64,
		.float_limit = 0,
		.timer_concatenation = 0
	};

	XMC_CCU4_SLICE_EVENT_CONFIG_t MANCHESTER_RX_TIMER_event_config =
	{
		.mapped_input = XMC_CCU4_SLICE_INPUT_C, // pin 2.13
		.edge = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_DUAL_EDGE,
		.level = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH,
		.duration = XMC_CCU4_SLICE_EVENT_FILTER_7_CYCLES
	};

	// initialise capture timer on both edges of incoming signal

	XMC_CCU4_EnableClock(GLOBAL_CCU4_DALI.module_ptr, 2);
	//pin2.13(CCU43IN2C) > event0 > sr2 > irq_hdlr_58
	XMC_CCU4_SLICE_CaptureInit(CCU43_CC42, &MANCHESTER_RX_TIMER_config);
	XMC_CCU4_EnableShadowTransfer(GLOBAL_CCU4_DALI.module_ptr, XMC_CCU4_SHADOW_TRANSFER_SLICE_2);
	XMC_CCU4_SLICE_Capture1Config(CCU43_CC42, XMC_CCU4_SLICE_EVENT_0);
	XMC_CCU4_SLICE_ConfigureEvent(CCU43_CC42, XMC_CCU4_SLICE_EVENT_0, &MANCHESTER_RX_TIMER_event_config);
	//XMC_CCU4_SLICE_EnableEvent(CCU43_CC42, XMC_CCU4_SLICE_IRQ_ID_EVENT0);
	XMC_CCU4_SLICE_SetInterruptNode(CCU43_CC42, XMC_CCU4_SLICE_IRQ_ID_EVENT0, XMC_CCU4_SLICE_SR_ID_2);

	//XMC_CCU4_SLICE_ClearTimer((XMC_CCU4_SLICE_t*)CCU43_CC42);

	NVIC_SetPriority(CCU43_2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),
            63U, 0U));
	NVIC_EnableIRQ(CCU43_2_IRQn);
}

