#include "debug.h"
#include "my_debug.h"

volatile char debugString[512] = "\0";
volatile int pos = 0;

void putDebugString(char* pattern, char* value)
{
	pos += sprintf(&debugString[pos], pattern, value);	
}


void putDebugUInt32(char* pattern, uint32_t value)
{
	pos += sprintf(&debugString[pos], pattern, value);
}

void putDebugPtr(char* pattern, void* value)
{
	pos += sprintf(&debugString[pos], pattern, value);
}


void printDebugStringToUART()
{
	TRACE_INFO("%s\r\n", debugString); 
	clearDebugData();
}

void clearDebugData()
{
	pos = 0;
	sprintf(&debugString[pos], "%s", "\0");
	
}

int stderr_putchar (int ch)
{
return 0;
}
