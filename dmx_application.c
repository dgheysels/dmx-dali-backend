#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "os_port.h"

#include "debug.h"
#include "my_debug.h"

#include "dmx.h"
#include "dmx_application.h"

struct dmx_fixture fixtures[] =
{
	{
		.name = "fixture1",		
		.first_channel=1,
		.number_of_channels=4
	},
	{
		.name = "fixture2",
		.first_channel=5,
		.number_of_channels=4
	}
};

static void dmx_send(void*);

extern osMailQId mqttIn_DMX_id;

OsTask* dmx_send_task;

static void copy_channels(uint8_t* channels, char* data);
static struct dmx_fixture* retrieve_fixture(char* fixture_name);

void dmx_initialize_application(void)
{
	dmx_initialize();
	dmx_send_task = osCreateTask("DMX SEND", dmx_send, NULL, 50, OS_TASK_PRIORITY_NORMAL);
}

void dmx_register_fixture(char* fixture_name, int first_channel, int number_channels)
{
	/* NOT IMPLEMENTED - hardcoded fixtures are used*/
}

void dmx_send(void* args)
{
	while (1)
	{
		osEvent mqttIn_mail = osMailGet(mqttIn_DMX_id, osWaitForever);
		if (mqttIn_mail.status == osEventMail)
		{
			mqttIn_DMX_Data* in = (mqttIn_DMX_Data*)mqttIn_mail.value.p;
			
			struct dmx_fixture* fixture = retrieve_fixture(in->fixture_name);
			
			uint8_t* data = malloc(sizeof(uint8_t)*fixture->number_of_channels);
			copy_channels(data, in->channels);
						
			free(in->channels);
			osMailFree(mqttIn_DMX_id, in);
					
			dmx_send_frame(data, fixture->first_channel, fixture->number_of_channels);								
		}
	}
}

static void copy_channels(uint8_t* channels, char* data)
{	
	char* data_ctx;
	char* channel_ctx;	
	const char* channel;
	int i=0;
	
	channel = strtok_r(data, "-", &data_ctx);
	while (channel != NULL)
	{
		channels[i++] = strtoul(channel, &channel_ctx, 10);
		channel = strtok_r(NULL, "-", &data_ctx);
	}	
}

static struct dmx_fixture* retrieve_fixture(char* fixture_name)
{
	for (int i=0; i<sizeof(fixtures)/sizeof(struct dmx_fixture); i++)
	{
		if (!strcmp(fixtures[i].name, fixture_name))
		{
			return &fixtures[i];
		}
	}	
	return NULL;
}
