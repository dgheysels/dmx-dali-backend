/**
 * @file tls_config.h
 * @brief CycloneSSL configuration file
 *
 * @section License
 *
 * Copyright (C) 2010-2016 Oryx Embedded SARL. All rights reserved.
 *
 * This file is part of CycloneSSL Open.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @author Oryx Embedded SARL (www.oryx-embedded.com)
 * @version 1.7.4
 **/

#ifndef _TLS_CONFIG_H
#define _TLS_CONFIG_H

//Dependencies
#include "RTE_Components.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***

// <o>Trace level
// <i>Set the desired debugging level
// <i>Default: Info
// <0=>Off
// <1=>Fatal
// <2=>Error
// <3=>Warning
// <4=>Info
// <5=>Debug
#define TLS_TRACE_LEVEL 2

// <o>Minimum SSL/TLS version
// <i>Minimum version that can be negotiated
// <i>Default: SSL 3.0
// <0x0300=>SSL 3.0
// <0x0301=>TLS 1.0
// <0x0302=>TLS 1.1
// <0x0303=>TLS 1.2
#define TLS_MIN_VERSION 0x0300

// <o>Maximum SSL/TLS version
// <i>Maximum version that can be negotiated
// <i>Default: TLS 1.2
// <0x0300=>SSL 3.0
// <0x0301=>TLS 1.0
// <0x0302=>TLS 1.1
// <0x0303=>TLS 1.2
#define TLS_MAX_VERSION 0x0303

// <q>Use BSD socket API
// <i>Enable SSL/TLS support
// <i>Default: Disabled
#define TLS_BSD_SOCKET_SUPPORT 0

// <q>Session resumption
// <i>Enable session resumption mechanism
// <i>Default: Enabled
#define TLS_SESSION_RESUME_SUPPORT 1

// <o>Session cache entry lifetime
// <i>Lifetime of session cache entries (in seconds)
// <i>Default: 3600
// <10-86400>
// <#*1000>
#define TLS_SESSION_CACHE_LIFETIME 3600000

// <q>SNI extension
// <i>Enable SNI (Server Name Indication) extension
// <i>Default: Enabled
#define TLS_SNI_SUPPORT ENABLED

// <o>Maximum number of certificates
// <i>Maximum number of certificates the end entity can load
// <i>Default: 3
// <1-8>
#define TLS_MAX_CERTIFICATES 3

// <o>Maximum message length
// <i>Maximum message length that can be handled by the higher-level protocol
// <i>Default: 16384
// <1024-65536>
#define TLS_MAX_PROTOCOL_DATA_LENGTH 16384

//SSL/TLS support
#ifdef RTE_CYCLONE_SSL
   #define TLS_SUPPORT ENABLED
#else
   #define TLS_SUPPORT DISABLED
#endif

//Client mode of operation
#ifdef RTE_CYCLONE_SSL_CLIENT
   #define TLS_CLIENT_SUPPORT ENABLED
#else
   #define TLS_CLIENT_SUPPORT DISABLED
#endif

//Server mode of operation
#ifdef RTE_CYCLONE_SSL_SERVER
   #define TLS_SERVER_SUPPORT ENABLED
#else
   #define TLS_SERVER_SUPPORT DISABLED
#endif

//RSA key exchange support
#ifdef RTE_CYCLONE_SSL_RSA
   #define TLS_RSA_SUPPORT ENABLED
#else
   #define TLS_RSA_SUPPORT DISABLED
#endif

//DHE_RSA key exchange support
#ifdef RTE_CYCLONE_SSL_DHE_RSA
   #define TLS_DHE_RSA_SUPPORT ENABLED
#else
   #define TLS_DHE_RSA_SUPPORT DISABLED
#endif

//DHE_DSS key exchange support
#ifdef RTE_CYCLONE_SSL_DHE_DSS
   #define TLS_DHE_DSS_SUPPORT ENABLED
#else
   #define TLS_DHE_DSS_SUPPORT DISABLED
#endif

//DH_anon key exchange support
#ifdef RTE_CYCLONE_SSL_DH_ANON
   #define TLS_DH_ANON_SUPPORT ENABLED
#else
   #define TLS_DH_ANON_SUPPORT DISABLED
#endif

//ECDHE_RSA key exchange support
#ifdef RTE_CYCLONE_SSL_ECDHE_RSA
   #define TLS_ECDHE_RSA_SUPPORT ENABLED
#else
   #define TLS_ECDHE_RSA_SUPPORT DISABLED
#endif

//ECDHE_ECDSA key exchange support
#ifdef RTE_CYCLONE_SSL_ECDHE_ECDSA
   #define TLS_ECDHE_ECDSA_SUPPORT ENABLED
#else
   #define TLS_ECDHE_ECDSA_SUPPORT DISABLED
#endif

//ECDH_anon key exchange support
#ifdef RTE_CYCLONE_SSL_ECDH_ANON
   #define TLS_ECDH_ANON_SUPPORT ENABLED
#else
   #define TLS_ECDH_ANON_SUPPORT DISABLED
#endif

//RSA signature capability
#ifdef RTE_CYCLONE_SSL_RSA_SIGN
   #define TLS_RSA_SIGN_SUPPORT ENABLED
#else
   #define TLS_RSA_SIGN_SUPPORT DISABLED
#endif

//DSA signature capability
#ifdef RTE_CYCLONE_SSL_DSA_SIGN
   #define TLS_DSA_SIGN_SUPPORT ENABLED
#else
   #define TLS_DSA_SIGN_SUPPORT DISABLED
#endif

//ECDSA signature capability
#ifdef RTE_CYCLONE_SSL_ECDSA_SIGN
   #define TLS_ECDSA_SIGN_SUPPORT ENABLED
#else
   #define TLS_ECDSA_SIGN_SUPPORT DISABLED
#endif

//Stream cipher support
#ifdef RTE_CYCLONE_SSL_RC4
   #define TLS_STREAM_CIPHER_SUPPORT ENABLED
#else
   #define TLS_STREAM_CIPHER_SUPPORT DISABLED
#endif

//CBC block cipher support
#ifdef RTE_CYCLONE_SSL_CBC
   #define TLS_CBC_CIPHER_SUPPORT ENABLED
#else
   #define TLS_CBC_CIPHER_SUPPORT DISABLED
#endif

//CCM AEAD support
#ifdef RTE_CYCLONE_SSL_CCM
   #define TLS_CCM_CIPHER_SUPPORT ENABLED
#else
   #define TLS_CCM_CIPHER_SUPPORT DISABLED
#endif

//GCM AEAD support
#ifdef RTE_CYCLONE_SSL_GCM
   #define TLS_GCM_CIPHER_SUPPORT ENABLED
#else
   #define TLS_GCM_CIPHER_SUPPORT DISABLED
#endif

//ChaCha20Poly1305 AEAD support
#ifdef RTE_CYCLONE_SSL_CHACHA20POLY1305
   #define TLS_CHACHA20_POLY1305_SUPPORT ENABLED
#else
   #define TLS_CHACHA20_POLY1305_SUPPORT DISABLED
#endif

//RC4 cipher support
#ifdef RTE_CYCLONE_SSL_RC4
   #define TLS_RC4_SUPPORT ENABLED
#else
   #define TLS_RC4_SUPPORT DISABLED
#endif

//IDEA cipher support
#ifdef RTE_CYCLONE_SSL_IDEA
   #define TLS_IDEA_SUPPORT ENABLED
#else
   #define TLS_IDEA_SUPPORT DISABLED
#endif

//DES cipher support
#ifdef RTE_CYCLONE_SSL_DES
   #define TLS_DES_SUPPORT ENABLED
#else
   #define TLS_DES_SUPPORT DISABLED
#endif

//Triple DES cipher support
#ifdef RTE_CYCLONE_SSL_3DES
   #define TLS_3DES_SUPPORT ENABLED
#else
   #define TLS_3DES_SUPPORT DISABLED
#endif

//AES cipher support
#ifdef RTE_CYCLONE_SSL_AES
   #define TLS_AES_SUPPORT ENABLED
#else
   #define TLS_AES_SUPPORT DISABLED
#endif

//Camellia cipher support
#ifdef RTE_CYCLONE_SSL_CAMELLIA
   #define TLS_CAMELLIA_SUPPORT ENABLED
#else
   #define TLS_CAMELLIA_SUPPORT DISABLED
#endif

//SEED cipher support
#ifdef RTE_CYCLONE_SSL_SEED
   #define TLS_SEED_SUPPORT ENABLED
#else
   #define TLS_SEED_SUPPORT DISABLED
#endif

//ARIA cipher support
#ifdef RTE_CYCLONE_SSL_ARIA
   #define TLS_ARIA_SUPPORT ENABLED
#else
   #define TLS_ARIA_SUPPORT DISABLED
#endif

//MD5 hash support
#ifdef RTE_CYCLONE_SSL_MD5
   #define TLS_MD5_SUPPORT ENABLED
#else
   #define TLS_MD5_SUPPORT DISABLED
#endif

//SHA-1 hash support
#ifdef RTE_CYCLONE_SSL_SHA1
   #define TLS_SHA1_SUPPORT ENABLED
#else
   #define TLS_SHA1_SUPPORT DISABLED
#endif

//SHA-224 hash support
#ifdef RTE_CYCLONE_SSL_SHA224
   #define TLS_SHA224_SUPPORT ENABLED
#else
   #define TLS_SHA224_SUPPORT DISABLED
#endif

//SHA-256 hash support
#ifdef RTE_CYCLONE_SSL_SHA256
   #define TLS_SHA256_SUPPORT ENABLED
#else
   #define TLS_SHA256_SUPPORT DISABLED
#endif

//SHA-384 hash support
#ifdef RTE_CYCLONE_SSL_SHA384
   #define TLS_SHA384_SUPPORT ENABLED
#else
   #define TLS_SHA384_SUPPORT DISABLED
#endif

//SHA-512 hash support
#ifdef RTE_CYCLONE_SSL_SHA512
   #define TLS_SHA512_SUPPORT ENABLED
#else
   #define TLS_SHA512_SUPPORT DISABLED
#endif

//secp160k1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP160K1
   #define TLS_SECP160K1_SUPPORT ENABLED
#else
   #define TLS_SECP160K1_SUPPORT DISABLED
#endif

//secp160r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP160R1
   #define TLS_SECP160R1_SUPPORT ENABLED
#else
   #define TLS_SECP160R1_SUPPORT DISABLED
#endif

//secp160r2 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP160R2
   #define TLS_SECP160R2_SUPPORT ENABLED
#else
   #define TLS_SECP160R2_SUPPORT DISABLED
#endif

//secp192k1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP192K1
   #define TLS_SECP192K1_SUPPORT ENABLED
#else
   #define TLS_SECP192K1_SUPPORT DISABLED
#endif

//secp192r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP192R1
   #define TLS_SECP192R1_SUPPORT ENABLED
#else
   #define TLS_SECP192R1_SUPPORT DISABLED
#endif

//secp224k1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP224K1
   #define TLS_SECP224K1_SUPPORT ENABLED
#else
   #define TLS_SECP224K1_SUPPORT DISABLED
#endif

//secp224r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP224R1
   #define TLS_SECP224R1_SUPPORT ENABLED
#else
   #define TLS_SECP224R1_SUPPORT DISABLED
#endif

//secp256k1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP256K1
   #define TLS_SECP256K1_SUPPORT ENABLED
#else
   #define TLS_SECP256K1_SUPPORT DISABLED
#endif

//secp256r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP256R1
   #define TLS_SECP256R1_SUPPORT ENABLED
#else
   #define TLS_SECP256R1_SUPPORT DISABLED
#endif

//secp384r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP384R1
   #define TLS_SECP384R1_SUPPORT ENABLED
#else
   #define TLS_SECP384R1_SUPPORT DISABLED
#endif

//secp521r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_SECP521R1
   #define TLS_SECP521R1_SUPPORT ENABLED
#else
   #define TLS_SECP521R1_SUPPORT DISABLED
#endif

//brainpoolP256r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_BRAINPOOLP256R1
   #define TLS_BRAINPOOLP256R1_SUPPORT ENABLED
#else
   #define TLS_BRAINPOOLP256R1_SUPPORT DISABLED
#endif

//brainpoolP384r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_BRAINPOOLP384R1
   #define TLS_BRAINPOOLP384R1_SUPPORT ENABLED
#else
   #define TLS_BRAINPOOLP384R1_SUPPORT DISABLED
#endif

//brainpoolP512r1 elliptic curve support
#ifdef RTE_CYCLONE_SSL_BRAINPOOLP512R1
   #define TLS_BRAINPOOLP512R1_SUPPORT ENABLED
#else
   #define TLS_BRAINPOOLP512R1_SUPPORT DISABLED
#endif

#endif
